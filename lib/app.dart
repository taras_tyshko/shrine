import 'package:flutter/material.dart';

import 'home.dart';
import 'login.dart';
import 'backdrop.dart';
import 'colors.dart';
import 'category_menu_page.dart';
import 'model/product.dart';
import 'supplemental/cut_corners_border.dart';

// TODO: Convert ShrineApp to stateful widget (104)

class ShrineApp extends StatefulWidget {
  ShrineApp();

  factory ShrineApp.forDesignTime() {
    // TODO: add arguments
    return new ShrineApp();
  }

  @override
  _ShrineAppState createState() => _ShrineAppState();
}

class _ShrineAppState extends State<ShrineApp> {
  Category _currentCategory = Category.all;

  void _onCategoryTap(Category category) {
    setState(() {
      _currentCategory = category;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shrine',
      // TODO: Change home.dart: to a Backdrop with a HomePage frontLayer (104)
      home: Backdrop(
          // TODO: Make currentCategory field take _currentCategory (104)
          currentCategory: _currentCategory,
          // TODO: Pass _currentCategory for frontLayer (104)
          frontLayer: HomePage(category: _currentCategory),
          // TODO: Change backLayer field value to CategoryMenuPage (104)
          backLayer: CategoryMenuPage(
              currentCategory: _currentCategory, onCategoryTap: _onCategoryTap),
          frontTitle: Text('SHRINE'),
          backTitle: Text('MENU')),
      initialRoute: '/login',
      onGenerateRoute: _getRoute,
      // TODO: Add a theme (103)
      theme: _kShrineTheme,
    );
  }

  Route<dynamic> _getRoute(RouteSettings settings) {
    if (settings.name != '/login') {
      return null;
    }

    return MaterialPageRoute<void>(
      settings: settings,
      builder: (BuildContext context) => LoginPage(),
      fullscreenDialog: true,
    );
  }
}

// TODO: Build a Shrine Theme (103)
final ThemeData _kShrineTheme = _buildShrineTheme();

ThemeData _buildShrineTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
      accentColor: kShrineBrown900,
      primaryColor: kShrinePink100,
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: kShrinePink100,
        textTheme: ButtonTextTheme.normal,
      ),
      scaffoldBackgroundColor: kShrineSurfaceWhite,
      cardColor: kShrineSurfaceWhite,
      textSelectionColor: kShrinePink100,
      errorColor: kShrineErrorRed,
      // TODO: Add the text themes (103)
      textTheme: _buildShrineTextTheme(base.textTheme),
      primaryTextTheme: _buildShrineTextTheme(base.primaryTextTheme),
      accentTextTheme: _buildShrineTextTheme(base.accentTextTheme),
      // TODO: Add the icon themes (103)
      primaryIconTheme: base.iconTheme.copyWith(color: kShrineBrown900),
      inputDecorationTheme: InputDecorationTheme(
        border: CutCornersBorder(),
      ));
}

// TODO: Build a Shrine Text Theme (103)
TextTheme _buildShrineTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline: base.headline.copyWith(
          fontWeight: FontWeight.w500,
        ),
        title: base.title.copyWith(
          fontSize: 18.0,
        ),
        caption: base.caption.copyWith(
          fontWeight: FontWeight.w400,
          fontSize: 14.0,
        ),
      )
      .apply(
        fontFamily: 'Rubik',
        displayColor: kShrineBrown900,
        bodyColor: kShrineBrown900,
      );
}
